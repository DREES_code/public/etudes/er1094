/* Copyright (C) 2018. Logiciel �labor� par l��tat, via la Drees.

Auteurs : Marianne Muller et Delphine Roy, Drees.

Ce programme informatique a �t� d�velopp� par la Drees. Il permet de produire les illustrations de la publication n�1094
de la collection �tudes et r�sultats, "L�Ehpad, dernier lieu de vie pour un quart des personnes d�c�d�es en France en 2015".

Le texte et les tableaux de l�article peuvent �tre consult�s sur le site de la DREES : 
https://drees.solidarites-sante.gouv.fr/etudes-et-statistiques/publications/etudes-et-resultats/article/l-ehpad-dernier-lieu-de-vie-pour-un-quart-des-personnes-decedees-en-france-en

Ce programme utilise les donn�es des �ditions 2011 et 2015 de l'enqu�te EHPA de la DREES. 
Dans les donn�es utilis�es, les �tablissements sont rep�r�s par leur num�ro FINESS (variable finessetab), qui n'est diffus� que via le CASD. 
Les analyses peuvent toutefois �tre r�pliqu�es sur les donn�es diffus�es via la plateforme PROGEDO (r�seau Quetelet), en rempla�ant cette variable par le num�ro d'�tablissement anonymis�.

Bien qu�il n�existe aucune obligation l�gale � ce sujet, les utilisateurs de ce programme sont invit�s � 
signaler � la DREES leurs travaux issus de la r�utilisation de ce code, ainsi que les �ventuels probl�mes 
ou anomalies qu�ils y rencontreraient, en �crivant � l�adresse �lectronique DREES-CODE@sante.gouv.fr

Ce programme a �t� ex�cut� pour la derni�re fois le 03/09/2020 avec la version 9.4 de SAS.

================================================================================================

# LICENCE : Ce logiciel est r�gi par la licence �GNU General Public License� GPL-3.0. 
# https://spdx.org/licenses/GPL-3.0.html#licenseText

# � cet �gard l'attention de l'utilisateur est attir�e sur les risques associ�s au chargement, � l'utilisation,
# � la modification et/ou au d�veloppement et � la reproduction du logiciel par l'utilisateur �tant donn� sa 
# sp�cificit� de logiciel libre, qui peut le rendre complexe � manipuler et qui le r�serve donc � des 
# d�veloppeurs et des professionnels avertis poss�dant des connaissances informatiques approfondies. 
# Les utilisateurs sont donc invit�s � charger et tester l'ad�quation du logiciel � leurs besoins dans des 
# conditions permettant d'assurer la s�curit� de leurs syst�mes et ou de leurs donn�es et, plus g�n�ralement, 
# � l'utiliser et l'exploiter dans les m�mes conditions de s�curit�.

# Le fait que vous puissiez acc�der � cet en-t�te signifie que vous avez pris connaissance de la licence GPL-3.0, et que vous en avez accept� les termes.

# This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
# This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
# You should have received a copy of the GNU General Public License along with this program.If not, see <https://www.gnu.org/licenses/>.

================================================================================================ */

/*********************************************************************************************************************************************/

libname EHPA "[]\Bases nat avec pond�ration r�gionale_sept 2017";
libname EHPA2011 "[]\Bases 2011 avec CATEG_2";


/*****************************************************************************************************************************************************/
/****************************** ER 1094 - programme 1 - CREATION DE LA BASE ET STATISTIQUES DESCRIPTIVES SUR LES RESIDENTS ***************************/
/*****************************************************************************************************************************************************/


/* champ : EHPAD uniquement */
data acti; set ehpa.f2_acti; 
where categ_2 in ("EHPAD priv�s � but lucratif" "EHPAD priv�s � but non lucratif" "EHPAD publics hospitaliers" "EHPAD publics non hospitaliers");
run;
/* A noter : Concernant les effectifs accueillis/sortis (EFFTOT), ne prendre que ceux en h�bergement permanent */
/* 16 �tablissements ne font pas d'accueil en h�bergement permanent : ils sortent donc du champ */

/* R�sidents en EHPAD, en h�bergement permanent */
data acc; set ehpa.f4_acc; 
where categ_2 in ("EHPAD priv�s � but lucratif" "EHPAD priv�s � but non lucratif" "EHPAD publics hospitaliers" "EHPAD publics non hospitaliers") and HBTG4="1";
run;

/* R�sidents sortis dans l'ann�e d'un EHPAD, en h�bergement permanent */
/* A noter : pour pouvoir comparer les r�sultats, on laisse les sorties autres que d�c�s. */
/* Pour rep�rer les d�c�s : where motsor in ("1" "2" "3") */

data sor; set ehpa.f5_sor; 
where categ_2 in ("EHPAD priv�s � but lucratif" "EHPAD priv�s � but non lucratif" "EHPAD publics hospitaliers" "EHPAD publics non hospitaliers") and HBGT5="1";
run;


/*** 2. idem sur 2011 ***/
data ett2011; set ehpa2011.ett_2011; 
where categ_2 in ("EHPAD priv�s � but lucratif" "EHPAD priv�s � but non lucratif" "EHPAD publics hospitaliers" "EHPAD publics non hospitaliers");
run;

data acti2011; set ehpa2011.acti_2011; 
where categ_2 in ("EHPAD priv�s � but lucratif" "EHPAD priv�s � but non lucratif" "EHPAD publics hospitaliers" "EHPAD publics non hospitaliers");
run;

data acc2011; set ehpa2011.acc_2011; 
where categ_2 in ("EHPAD priv�s � but lucratif" "EHPAD priv�s � but non lucratif" "EHPAD publics hospitaliers" "EHPAD publics non hospitaliers") and HBTG4="1";
run;

data sor2011; set ehpa2011.sor_2011; 
where categ_2 in ("EHPAD priv�s � but lucratif" "EHPAD priv�s � but non lucratif" "EHPAD publics hospitaliers" "EHPAD publics non hospitaliers") and HBGT5="1";
run;


/********************************** Formats ******************************************/

proc format; 
value $gir
"1"="1"
"2"="2"
"3"="3"
"4"="4"
"5"="5"
"6"="6"
"  ","9"="Non renseign�";

value $sexe
"1"="1 Hommes"
"2"="2 Femmes"
" "="3 Non renseign� "; 

value $HBTG
"1"="H�bergement permanent"
"2"="H�bergement temporaire"
"3"="Accueil de jour"
"4"="Accueil de nuit";

value $motsor
"1"="1A	D�c�s dans l��tablissement" 
"2"="1B	D�c�s lors d�une hospitalisation" 
"3"="1C	D�c�s lors d�une autre sortie temporaire"
"4"="2	D�part volontaire � l�initiative du r�sident ou d�un proche"
"5"="3	Fin d'un s�jour volontaire � l'initiative du r�sident ou d'un proche"
"6"="4	R�siliation du Contrat de s�jour � l�initiative de l�ET_inadaptation de l��tat de sant�" 
"7"="5	R�siliation du Contrat de s�jour � l�initiative de l�ET_d�faut de paiement" 
"8"="6	Autre"
" "="Non renseign�";

value $type
"01"="Domicile priv� ou d�un proche"
"02"="Accueil familial agr��"
"03"="Logement-foyer"
"04"="Maison de retraite non EHPAD"
"05"="Maison de retraite m�dicalis�e, EHPAD (hors UHR)"
"06"="UHR d'une maison de retraite m�dicalis�e ou d'un EHPAD"
"07"="Soins de longue dur�e"
"08"="Service de soins de suite et de r�adaptation d�un �tablissement de sant� (ex moyen s�jour) (hors UCC)"
"09"="Unit� cognitivo-comportementale (UCC)"
"10"="Unit� de court s�jour (�m�decine, chirurgie�)"
"11"="�tablissement psychiatrique ou service psychiatrique d�un �tablissement de sant�"
"12"="�tablissement pour adultes handicap�s"
"13"="Autre"
"99"="Ne sait pas"
""="Non renseign�";

value $DEST 
""="Non r�ponse"
"01"="Domicile priv� ou d�un proche"
"02"="Accueil familial agr��"
"03"="Logement-foyer"
"04"="Maison de retraite non EHPAD"
"05"="Maison de retraite m�dicalis�e, EHPAD (hors UHR)"
"06"="UHR d'une maison de retraite m�dicalis�e ou d'un EHPAD"
"07"="Soins de longue dur�e"
"08"="Service de soins de suite et de r�adaptation d�un �tablissement de sant� (ex moyen s�jour) (hors UCC)"
"09"="Unit� cognitivo-comportementale (UCC)"
"10"="Unit� de court s�jour (�m�decine, chirurgie�)"
"11"="�tablissement psychiatrique ou service psychiatrique d�un �tablissement de sant�"
"12"="Autre"
"99"="Ne sait pas";

run;

/*****************************************************************************************************************************************************/
/******************************************************** 2. PARTIE DESCRIPTIVE ************************************************************************/
/*****************************************************************************************************************************************************/

/*******************  Graphique web ************************/

data sor_graph; 
set sor; 

length TRDUREE_2 $15.;
if (DUREE5 ne . and DUREE5<6) then TRDUREE_2="1. < 6 mois";
else if (DUREE5>=6 and DUREE5<12) then TRDUREE_2="2. 6 mois � 1 an";
else if (DUREE5>=12 and DUREE5<24) then TRDUREE_2="3. 1 � 2 ans";
else if (DUREE5>=24 and DUREE5<36) then TRDUREE_2="4. 2 � 3 ans";
else if (DUREE5>=36 and DUREE5<48) then TRDUREE_2="5. 3 � 4 ans";
else if DUREE5>=48 then TRDUREE_2="6. >= 4 ans";
if DUREE5=. then TRDUREE_2="";
run; 


proc freq data=sor_graph; 
table TRDUREE_2*motsor/nopercent nocum nocol nofreq;
weight poids5;
run;


/* TABLEAU 1 */
/*********************		2015		*************************/

proc sort data=acti; by categ_2; run;
proc sort data=sor; by categ_2; run;


/* Nombre de personnes accueillies au cours de l'ann�e 2015 */
proc means data=acti sum;
var sortper sortemp sorjour sornuit sortot;
class categ_2;
weight poids2; 

run; 

/* Nombre de d�c�s en EHPAD en 2015 */
proc freq data=sor; 
table motsor/ nocum; 
weight poids5;
format motsor $motsor.;
run; 

proc freq data=sor; 
table motsor*categ/norow nopercent nofreq nocum; 
weight poids5;
format motsor $motsor.;
run;


proc freq data=sor; 
table dest/nocum; 
where motsor not in ("" "99" "1" "2" "3");
weight poids5;
format dest $dest.;
run;

/*Nb de d�c�s des 75ans et + */
proc freq data=sor; 
table TRAGESOR5*MOTSOR / nopercent nocol norow;
where motsor in ("1", "2", "3");
weight poids5;
format motsor $motsor.;
run;

proc freq data=sor; 
table motsor; 
weight poids5;
run; 

/*********************		2011		*************************/

proc sort data=acti2011; by categ_2; run;
proc sort data=sor2011; by categ_2; run;

/* Nombre de personnes accueillies au cours de l'ann�e 2015 */
proc means data=acti2011 sum;
/*var effhp efftemp effjour effnuit efftot;*/
var sortper sortemp sorjour sornuit sortot;
/*class categ_2;*/
weight poids2; 
run; 

/* Nombre de d�c�s en EHPAD en 2015 */
proc freq data=sor2011; 
table motsor; 
weight poids5;
format motsor $motsor.;
run; 

proc freq data=sor2011; 
table motsor*categ/norow nopercent nofreq nocum; 
weight poids5;
format motsor $motsor.;
run;

/*Nb de d�c�s des 75ans et + */
proc freq data=sor2011; 
table TRAGESOR5;
/*where TRAGESOR5 not in ("" "9 - Non renseign�") */
weight poids5;
format motsor $motsor.;
run;


	/****************************************** PROFIL DES DECEDES **************************************************/

/* Nombre de d�c�s selon le sexe des d�c�d�s*/
proc freq data=sor; 
table sexe5/nocum ; 
weight poids5;
where motsor in ("1" "2" "3");
run; 
/*NR=0.06%*/

proc freq data=sor; 
table motsor*sexe5/norow nopercent nofreq nocum; 
weight poids5;
format motsor $motsor. sexe5 $sexe.;
run; 

/* Nombre de d�c�s selon le sexe et l'�ge des d�c�d�s*/
proc freq data=sor; 
table TRAGESOR5*sexe5/missing nocum nocol norow nopercent; 
weight poids5;
where motsor in ("1" "2" "3") ;
run; 

data sor_1;
set sor;
AGESOR5_AN=INT(AGESOR5/12);
if AGESOR5=. then AGESOR5_AN=.;
run; 

 /* GRAPHIQUE 1 */
/* Nombre de d�c�s selon l'�ge des d�c�d�s */
proc freq data=sor_1; 
table AGESOR5_AN*sexe5/missing nocum nocol norow nopercent; 
weight poids5;
where motsor in ("1" "2" "3") /*and cat="500"*/;
*by cat;
run; 

 /* GRAPHIQUE 1 */
/* Diff�rence avec la structure des persnnes accueillies */

proc freq data=acc ; 
table TRAGE4*sexe4/nocum nocol norow nopercent;
weight poids4;
run;


data acc_1;
set acc;
AGE4_AN=INT(AGE4/12);
if AGE4=. then AGE4_AN=.;
run; 

proc freq data=acc_1; 
table AGE4_AN*sexe4/missing nocum nocol norow nopercent; 
weight poids4;
run; 

/*�ge moyen au d�c�s selon le sexe */
proc means data=sor_1 mean q1 median q3 ; 
var AGESOR5;
*class sexe5;
where motsor in ("1" "2" "3");
weight poids5; 
run;

/* �ge � la sortie selon le motif de sortie */
proc means data=sor_1 mean q1 median q3 ; 
var AGESOR5;
*class motsor;
where motsor in ("4" "5" "6" "7" "8");
*format motsor $motsor.;
weight poids5; 
run;

/*Age � la sortie selon le type d'h�bergement ant�rieur */
proc means data=sor_1 mean q1 median q3 ; 
var AGESOR5;
class type5;
where motsor in ("1" "2" "3");
*format motsor $motsor.;
weight poids5; 
run;


proc freq data=sor; 
table motsor*TRAGESOR5/missing nocum nocol norow nopercent; 
weight poids5;
format motsor $motsor.;
run; 

proc freq data=sor; 
table motsor/missing nocum nocol norow nopercent; 
weight poids5;
format motsor $motsor.;
run; 

/* Motif de sortie selon la cat�gorie */
proc freq data=sor; 
table motsor*categ_2/missing nocum nocol norow nopercent; 
weight poids5;
format motsor $motsor.;
run; 

proc freq data=sor; 
table dest*categ_2/missing nocum nocol norow nopercent; 
weight poids5;
format dest $dest.;
run; 


proc means data=sor mean q1 median q3; 
var AGESOR5;
class type5;
where motsor in ("1" "2" "3");
format type5 $type.;
weight poids5; 
run;

/* TABLEAU 2 */
/* Dur�e de s�jour des personnes selon leur motif de sortie;*/

proc means data=sor_1 mean q1 median q3; 
var DUREE5;
where motsor in ("4" "5" "6" "7" "8");
weight poids5; 
run;

/*Dur�e de s�jour des personnes d�c�d�es selon le sexe */
proc means data=sor_1 mean q1 median q3 ; 
var DUREE5;
class sexe5;
where motsor in ("1" "2" "3");
weight poids5; 
run;


/* Dur�e de s�jour des personnes d�c�d�es selon leur h�bergement ant�rieur */

proc means data=sor_1 mean q1 median q3; 
var DUREE5;
class TYPE5 /*cat*/;
weight poids5; 
format TYPE5 $TYPE.;
where motsor in ("1" "2" "3");
run;

/*Dur�e de s�jour moyenne en 2011; */

proc means data=sor2011 mean q1 median q3; 
var DUREE5;
weight poids5; 
run;


/* GIR */
proc freq data=sor; 
table GIR5/ nocum; 
weight poids5;
where motsor in ("1" "2" "3") and GIR5 ne "9";
run; 
/*0.07% de NR + 1.17 de NSP */

proc freq data=sor; 
table GIR5/ nocum; 
weight poids5;
where motsor in ("1" "2" "3") and GIR5 ne "9" and an_ent_c=2015;
/*personnes entr�es en 2015 et d�c�d�es dans l'ann�e */
run; 

/* Gir et r�sidence ant�rieure;*/
proc freq data=sor; 
table type5*GIR5/ nocum nocol nopercent norow; 
weight poids5;
/*where motsor in ("1" "2" "3")*/ /*and GIR5 ne "9" */
format type5 $type.;
format gir5 $gir.;
run; 

/* Comparaison avec personnes accueillies */

proc freq data=acc; 
table GIR4/nocum; 
where GIR4 ne "9";
weight poids4;
run; 
/*0.13% de NR + 0.47 de NSP */

proc freq data=sor; 
table hand5/missing nocum; 
*weight poids5;
where motsor in ("1" "2" "3");
run; 

/* Comparaison avec personnes d�c�d�es en 2011  */

proc means data=sor2011 mean q1 median q3 ; 
var AGESOR5;
class sexe5;
where motsor in ("1" "2" "3");
weight poids5; 
run;

proc freq data=sor2011; 
table GIR5/ nocum; 
weight poids5;
where motsor in ("1" "2" "3") and GIR5 ne "9";
run; 


/**** Profil des d�c�d�s en logements-foyers ***/
data sor_LF; set ehpa.f5_sor; 
where cat="202" and HBGT5="1"; run;

proc freq data=sor_lf;
table motsor; 
weight poids5;
format motsor $motsor.;
run; 

proc freq data=sor_lf; 
/*table GIR5/ nocum; */
table SEXE5/ nocum; 
weight poids5;
where motsor in ("1" "2" "3") /*and GIR5 ne "9"*/;
run; 

data sor_lf2;
set sor_lf;
AGESOR5_AN=INT(AGESOR5/12);
if AGESOR5=. then AGESOR5_AN=.;
run; 

/**�ge moyen au d�c�s selon le sexe */
proc means data=sor_lf2 mean q1 median q3 ; 
var AGESOR5;
class sexe5;
where motsor in ("1" "2" "3");
weight poids5; 
run;

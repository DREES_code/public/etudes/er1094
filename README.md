# ER1094

Ce dossier contient les programmes ayant servi à produire les illustrations ainsi que les chiffres cités dans le texte de l'Etudes et résultats n°1094 "L'EHPAD, dernier lieu de vie pour un quart des personnes décédées en France en 2015". 
Lien vers l'étude : https://drees.solidarites-sante.gouv.fr/etudes-et-statistiques/publications/etudes-et-resultats/article/l-ehpad-dernier-lieu-de-vie-pour-un-quart-des-personnes-decedees-en-france-en

Présentation de la DREES : La Direction de la recherche, des études, de l'évaluation et des statistiques (DREES) est le service statistique ministériel des ministères sanitaires et sociaux, et une direction de l'administration centrale de ces ministères.
https://drees.solidarites-sante.gouv.fr/article/presentation-de-la-drees

Source de données: enquêtes EHPA 2015 et EHPA 2011 (DREES). Calculs DREES.

Le code de cet "Études et résultats" est constitué de 4 programmes : 
- le programme 1 produit les tableaux 1, 2, A, B, C et le tableau 2, sur les résidents décédés, en 2011 et 2015
- le programme 2 produit les tableaux 3, 4, 5, D, sur les établissements
- le programme 3 crée une base destinée à réaliser les régressions 
- le programme 4 réalise les statistiques descriptives des tableaux complémentaires E et F, et les régressions dont les résultats sont présentés dans le tableau complémentaire G.

Les programmes ont été exécutés pour la dernière fois avec le logiciel SAS version 9.4, le 03/09/2020.

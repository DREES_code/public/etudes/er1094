/* Copyright (C) 2018. Logiciel �labor� par l��tat, via la Drees.

Auteurs : Marianne Muller et Delphine Roy, Drees.

Ce programme informatique a �t� d�velopp� par la Drees. Il permet de produire les illustrations de la publication n�1094
de la collection �tudes et r�sultats, "L�Ehpad, dernier lieu de vie pour un quart des personnes d�c�d�es en France en 2015".

Le texte et les tableaux de l�article peuvent �tre consult�s sur le site de la DREES : 
https://drees.solidarites-sante.gouv.fr/etudes-et-statistiques/publications/etudes-et-resultats/article/l-ehpad-dernier-lieu-de-vie-pour-un-quart-des-personnes-decedees-en-france-en

Ce programme utilise les donn�es des �ditions 2011 et 2015 de l'enqu�te EHPA de la DREES. 
Dans les donn�es utilis�es, les �tablissements sont rep�r�s par leur num�ro FINESS (variable finessetab), qui n'est diffus� que via le CASD. 
Les analyses peuvent toutefois �tre r�pliqu�es sur les donn�es diffus�es via la plateforme PROGEDO (r�seau Quetelet), 
en rempla�ant cette variable par le num�ro d'�tablissement anonymis�.

Il utilise �galement une variable DEGRE_UNIT pour classer les communes selon leur caract�re urbain ou rural, qui n'est actuellement pas diffus�e. 

Bien qu�il n�existe aucune obligation l�gale � ce sujet, les utilisateurs de ce programme sont invit�s � 
signaler � la DREES leurs travaux issus de la r�utilisation de ce code, ainsi que les �ventuels probl�mes 
ou anomalies qu�ils y rencontreraient, en �crivant � l�adresse �lectronique DREES-CODE@sante.gouv.fr

Ce programme a �t� ex�cut� pour la derni�re fois le 03/09/2020 avec la version 9.4 de SAS.

================================================================================================

# LICENCE : Ce logiciel est r�gi par la licence �GNU General Public License� GPL-3.0. 
# https://spdx.org/licenses/GPL-3.0.html#licenseText

# � cet �gard l'attention de l'utilisateur est attir�e sur les risques associ�s au chargement, � l'utilisation,
# � la modification et/ou au d�veloppement et � la reproduction du logiciel par l'utilisateur �tant donn� sa 
# sp�cificit� de logiciel libre, qui peut le rendre complexe � manipuler et qui le r�serve donc � des 
# d�veloppeurs et des professionnels avertis poss�dant des connaissances informatiques approfondies. 
# Les utilisateurs sont donc invit�s � charger et tester l'ad�quation du logiciel � leurs besoins dans des 
# conditions permettant d'assurer la s�curit� de leurs syst�mes et ou de leurs donn�es et, plus g�n�ralement, 
# � l'utiliser et l'exploiter dans les m�mes conditions de s�curit�.

# Le fait que vous puissiez acc�der � cet en-t�te signifie que vous avez pris connaissance de la licence GPL-3.0, et que vous en avez accept� les termes.

# This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
# This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
# You should have received a copy of the GNU General Public License along with this program.If not, see <https://www.gnu.org/licenses/>.

================================================================================================ */

/*********************************************************************************************************************************************************************************/
/*************************************	ER 1094 - programme 4 - mod�lisation de la probabilit� de mourir � l'h�pital plut�t que dans l'EHPAD  ************************************/
/*********************************************************************************************************************************************************************************/

libname EHPA "[]\Bases nat avec pond�ration r�gionale_sept 2017";
libname EHPA2011 "[]\Bases 2011 avec CATEG_2";
libname deces '[]\Bases ER1049';

data deces01b; set deces.deces01b; 
degreUbis_inc=(DEGRE_UNIT=.);
degreUbis_1=(DEGRE_UNIT=1);
degreUbis_2=(DEGRE_UNIT=2);
degreUbis_34=(DEGRE_UNIT in (3,4));

/*1- communes dens�ment peupl�es ;
2- communes de densit� interm�diaire ;
3- communes peu denses ;
4- communes tr�s peu denses. */
run;

%let varexp=deceshosp ; /* version 1, avec la variable expliqu�e seulement parmi les sorties pour d�ces, hosp VS EHPAD */
%let varexp=dcycSLDMCO; /* version 2, en test de robustesse � l'inclusion des sorties vers SLD et MCO dans la modalit�  "d�c�s � l'h�pital" */ 

proc freq data=deces01b; table deceshosp*dcycSLDMCO / missing; run;

/* 0 - la proc reg de d�part pour comparaison */
ods output ParameterEstimates = OLSbiais;

proc reg data=deces01b; 
model &varexp.= 
/**************** 1- caract�ristique du r�sident */
homme /*AGESOR5_AN AGECARRE */
AGE_m70 AGE_7079 AGE_8084 /*AGE_8589*/ AGE_9094 AGE_9599 AGE_p100 AGE_INC
GIRS1 GIRS2 /*GIRS3*/ GIRS4 GIRS5 GIRS6 GIRSNA 
handicap
asUHR asALZ asPVH asPASA /*asNONE*/ 
/*DUREE_ans 1688 missing g�n�r�s ici*/
duree_inc duree_02 duree_35 duree_611 duree_1218 /*duree_1928*/ duree_2938 duree_3949 duree_5067 duree_6896 duree_97p

/***************** 2 - caract�ristiques de l'�tab */
categPRL /*categPRNL*/ categPUH categPUNH
CONVSPOUI /*CONVSPNON*/ CONVSPSO
CONVHADOUI /*CONVHADNON*/ CONVHADSO 
INF24OUI /*INF24NON*/ INF24NR
DIFRECRUTOUI
/*propHAD propASH propALZ propPH*/ 
GMP_inc GMP_m600 GMP_600649 /*GMP_650699*/ GMP_700749 GMP_750799 GMP_800p 
PMP_inc PMP_Q4 PMP_Q3 /*PMP_Q2*/ PMP_Q1 
PUI_OUI
PASOINSCOM_OUI
PADEPCOM_OUI
ACCFV_OUI
PROETSOINSP_OUI
FVIE_OUI
CONVSOINSP_OUI
propASH_inc propASH_0 propASH_110 /*propASH_1120*/ propASH_2130 propASH_3150 propASH_50p
/*GMPcarre*/

/************** 3 - localisation */
/*degreUbis_inc*/ degreUbis_1 /*degreUbis_2*/ degreUbis_34; run; quit; 

/* 1 - regression OLS avec seules caracs individuelles */

ods output ParameterEstimates = parmsind;
proc reg data=deces01b; 
model &varexp.= 
/**************** 1- caract�ristique du r�sident */
homme /*AGESOR5_AN AGECARRE */
AGE_m70 AGE_7079 AGE_8084 /*AGE_8589*/ AGE_9094 AGE_9599 AGE_p100 AGE_INC
GIRS1 GIRS2 /*GIRS3*/ GIRS4 GIRS5 GIRS6 GIRSNA 
handicap
asUHR asALZ asPVH asPASA /*asNONE*/ 
/*DUREE_ans 1688 missing g�n�r�s ici*/
duree_inc duree_02 duree_35 duree_611 duree_1218 /*duree_1928*/ duree_2938 duree_3949 duree_5067 duree_6896 duree_97p
; run; quit; 


/* 2 - regression MCG avec seules caracs individuelles + r�gion, correction corr�lation r�sidus */
ods output ParameterEstimates = parmsindMCG;
proc reg data=deces01b; 
model &varexp.= 
/**************** 1- caract�ristique du r�sident */
homme /*AGESOR5_AN AGECARRE */
AGE_m70 AGE_7079 AGE_8084 /*AGE_8589*/ AGE_9094 AGE_9599 AGE_p100 AGE_INC
GIRS1 GIRS2 /*GIRS3*/ GIRS4 GIRS5 GIRS6 GIRSNA 
handicap
asUHR asALZ asPVH asPASA /*asNONE*/ 
/*DUREE_ans 1688 missing g�n�r�s ici*/
duree_inc duree_02 duree_35 duree_611 duree_1218 /*duree_1928*/ duree_2938 duree_3949 duree_5067 duree_6896 duree_97p
 
/************** 3 - localisation */
/*degreUbis_inc*/ degreUbis_1 /*degreUbis_2*/ degreUbis_34
/ spec
; run; quit; 

/* format pour que les logit prennent 0 comme modalit� de r�f�rence et non 1 */
   proc format;
         value reorder
               1 = 'Un'
               0 = 'Z�ro';
         run;


/* 3 - Avec tout, biais�, en LOGIT */
ods output ParameterEstimates = LOGITbiais;

PROC LOGISTIC DATA=deces01b;
format homme /*AGESOR5_AN AGECARRE */
AGE_m70 AGE_7079 AGE_8084 /*AGE_8589*/ AGE_9094 AGE_9599 AGE_p100 AGE_INC
GIRS1 GIRS2 /*GIRS3*/ GIRS4 GIRS5 GIRS6 GIRSNA 
handicap
asUHR asALZ asPVH asPASA /*asNONE*/ 
duree_inc duree_02 duree_35 duree_611 duree_1218 /*duree_1928*/ duree_2938 duree_3949 duree_5067 duree_6896 duree_97p
/***************** 2 - caract�ristiques de l'�tab */
categPRL /*categPRNL*/ categPUH categPUNH
CONVSPOUI /*CONVSPNON*/ CONVSPSO
CONVHADOUI /*CONVHADNON*/ CONVHADSO 
INF24OUI /*INF24NON*/ INF24NR
DIFRECRUTOUI
/*propHAD propASH propALZ propPH*/ 
GMP_inc GMP_m600 GMP_600649 /*GMP_650699*/ GMP_700749 GMP_750799 GMP_800p 
PMP_inc PMP_Q4 PMP_Q3 /*PMP_Q2*/ PMP_Q1 
PUI_OUI
PASOINSCOM_OUI
PADEPCOM_OUI
ACCFV_OUI
PROETSOINSP_OUI
FVIE_OUI
CONVSOINSP_OUI
propASH_inc propASH_0 propASH_110 /*propASH_1120*/ propASH_2130 propASH_3150 propASH_50p
/*GMPcarre*/
/************** 3 - localisation */
/*degreUbis_inc*/ degreUbis_1 /*degreUbis_2*/ degreUbis_34
reorder. ;

CLASS
homme /*AGESOR5_AN AGECARRE */
AGE_m70 AGE_7079 AGE_8084 /*AGE_8589*/ AGE_9094 AGE_9599 AGE_p100 AGE_INC
GIRS1 GIRS2 /*GIRS3*/ GIRS4 GIRS5 GIRS6 GIRSNA 
handicap
asUHR asALZ asPVH asPASA /*asNONE*/ 
/*DUREE_ans 1688 missing g�n�r�s ici*/
duree_inc duree_02 duree_35 duree_611 duree_1218 /*duree_1928*/ duree_2938 duree_3949 duree_5067 duree_6896 duree_97p
/***************** 2 - caract�ristiques de l'�tab */
categPRL /*categPRNL*/ categPUH categPUNH
CONVSPOUI /*CONVSPNON*/ CONVSPSO
CONVHADOUI /*CONVHADNON*/ CONVHADSO 
INF24OUI /*INF24NON*/ INF24NR
DIFRECRUTOUI
/*propHAD propASH propALZ propPH*/ 
GMP_inc GMP_m600 GMP_600649 /*GMP_650699*/ GMP_700749 GMP_750799 GMP_800p 
PMP_inc PMP_Q4 PMP_Q3 /*PMP_Q2*/ PMP_Q1 
PUI_OUI
PASOINSCOM_OUI
PADEPCOM_OUI
ACCFV_OUI
PROETSOINSP_OUI
FVIE_OUI
CONVSOINSP_OUI
propASH_inc propASH_0 propASH_110 /*propASH_1120*/ propASH_2130 propASH_3150 propASH_50p
/*GMPcarre*/
/************** 3 - localisation */
/*degreUbis_inc*/ degreUbis_1 /*degreUbis_2*/ degreUbis_34;

MODEL &varexp.(DESCENDING)  =homme /*AGESOR5_AN AGECARRE */
AGE_m70 AGE_7079 AGE_8084 /*AGE_8589*/ AGE_9094 AGE_9599 AGE_p100 AGE_INC
GIRS1 GIRS2 /*GIRS3*/ GIRS4 GIRS5 GIRS6 GIRSNA 
handicap
asUHR asALZ asPVH asPASA /*asNONE*/ 
/*DUREE_ans 1688 missing g�n�r�s ici*/
duree_inc duree_02 duree_35 duree_611 duree_1218 /*duree_1928*/ duree_2938 duree_3949 duree_5067 duree_6896 duree_97p

/***************** 2 - caract�ristiques de l'�tab */

categPRL /*categPRNL*/ categPUH categPUNH
CONVSPOUI /*CONVSPNON*/ CONVSPSO
CONVHADOUI /*CONVHADNON*/ CONVHADSO 
INF24OUI /*INF24NON*/ INF24NR
DIFRECRUTOUI
/*propHAD propASH propALZ propPH*/ 
GMP_inc GMP_m600 GMP_600649 /*GMP_650699*/ GMP_700749 GMP_750799 GMP_800p 
PMP_inc PMP_Q4 PMP_Q3 /*PMP_Q2*/ PMP_Q1 
PUI_OUI
PASOINSCOM_OUI
PADEPCOM_OUI
ACCFV_OUI
PROETSOINSP_OUI
FVIE_OUI
CONVSOINSP_OUI
propASH_inc propASH_0 propASH_110 /*propASH_1120*/ propASH_2130 propASH_3150 propASH_50p
/*GMPcarre*/

/************** 3 - localisation */
/*degreUbis_inc*/ degreUbis_1 /*degreUbis_2*/ degreUbis_34;
RUN;

/* 4 - idem en logit avec les seules caract�ristiques individuelles */

ods output ParameterEstimates = logistic;
PROC LOGISTIC DATA=deces01b;
format homme /*AGESOR5_AN AGECARRE */
AGE_m70 AGE_7079 AGE_8084 /*AGE_8589*/ AGE_9094 AGE_9599 AGE_p100 AGE_INC
GIRS1 GIRS2 /*GIRS3*/ GIRS4 GIRS5 GIRS6 GIRSNA 
handicap
asUHR asALZ asPVH asPASA /*asNONE*/ 
/*DUREE_ans 1688 missing g�n�r�s ici*/
duree_inc duree_02 duree_35 duree_611 duree_1218 /*duree_1928*/ duree_2938 duree_3949 duree_5067 duree_6896 duree_97p
reorder. ;

CLASS
homme /*AGESOR5_AN AGECARRE */
AGE_m70 AGE_7079 AGE_8084 /*AGE_8589*/ AGE_9094 AGE_9599 AGE_p100 AGE_INC
GIRS1 GIRS2 /*GIRS3*/ GIRS4 GIRS5 GIRS6 GIRSNA 
handicap
asUHR asALZ asPVH asPASA /*asNONE*/ 
duree_inc duree_02 duree_35 duree_611 duree_1218 /*duree_1928*/ duree_2938 duree_3949 duree_5067 duree_6896 duree_97p;

MODEL &varexp.(DESCENDING)  = homme /*AGESOR5_AN AGECARRE */
AGE_m70 AGE_7079 AGE_8084 /*AGE_8589*/ AGE_9094 AGE_9599 AGE_p100 AGE_INC
GIRS1 GIRS2 /*GIRS3*/ GIRS4 GIRS5 GIRS6 GIRSNA 
handicap
asUHR asALZ asPVH asPASA /*asNONE*/ 
duree_inc duree_02 duree_35 duree_611 duree_1218 /*duree_1928*/ duree_2938 duree_3949 duree_5067 duree_6896 duree_97p;

RUN;


/* 5 - logistic avec effets fixes (logit conditionnel) ***************************************/

PROC SORT DATA=deces01b ; BY  finessetab; RUN;
ods output ParameterEstimates = logFE;
PROC LOGISTIC DATA=deces01b;
format homme /*AGESOR5_AN AGECARRE */
AGE_m70 AGE_7079 AGE_8084 /*AGE_8589*/ AGE_9094 AGE_9599 AGE_p100 AGE_INC
GIRS1 GIRS2 /*GIRS3*/ GIRS4 GIRS5 GIRS6 GIRSNA 
handicap
asUHR asALZ asPVH asPASA /*asNONE*/ 
/*DUREE_ans 1688 missing g�n�r�s ici*/
duree_inc duree_02 duree_35 duree_611 duree_1218 /*duree_1928*/ duree_2938 duree_3949 duree_5067 duree_6896 duree_97p
reorder. ;

CLASS
homme /*AGESOR5_AN AGECARRE */
AGE_m70 AGE_7079 AGE_8084 /*AGE_8589*/ AGE_9094 AGE_9599 AGE_p100 AGE_INC
GIRS1 GIRS2 /*GIRS3*/ GIRS4 GIRS5 GIRS6 GIRSNA 
handicap
asUHR asALZ asPVH asPASA /*asNONE*/ 
/*DUREE_ans 1688 missing g�n�r�s ici*/
duree_inc duree_02 duree_35 duree_611 duree_1218 /*duree_1928*/ duree_2938 duree_3949 duree_5067 duree_6896 duree_97p;

MODEL &varexp.(DESCENDING)  = homme /*AGESOR5_AN AGECARRE */
AGE_m70 AGE_7079 AGE_8084 /*AGE_8589*/ AGE_9094 AGE_9599 AGE_p100 AGE_INC
GIRS1 GIRS2 /*GIRS3*/ GIRS4 GIRS5 GIRS6 GIRSNA 
handicap
asUHR asALZ asPVH asPASA /*asNONE*/ 
/*DUREE_ans 1688 missing g�n�r�s ici*/
duree_inc duree_02 duree_35 duree_611 duree_1218 /*duree_1928*/ duree_2938 duree_3949 duree_5067 duree_6896 duree_97p;

STRATA Finessetab;

RUN;

/* 6 - logit � effets al�atoires **********************************************/

ods output ParameterEstimates = logRE COVPARMS=sigmachap ;
PROC GLIMMIX DATA=deces01b;
format 
homme /*AGESOR5_AN AGECARRE */
AGE_m70 AGE_7079 AGE_8084 /*AGE_8589*/ AGE_9094 AGE_9599 AGE_p100 AGE_INC
GIRS1 GIRS2 /*GIRS3*/ GIRS4 GIRS5 GIRS6 GIRSNA 
handicap
asUHR asALZ asPVH asPASA /*asNONE*/ 
/*DUREE_ans 1688 missing g�n�r�s ici*/
duree_inc duree_02 duree_35 duree_611 duree_1218 /*duree_1928*/ duree_2938 duree_3949 duree_5067 duree_6896 duree_97p
/***************** 2 - caract�ristiques de l'�tab */
categPRL /*categPRNL*/ categPUH categPUNH
CONVSPOUI /*CONVSPNON*/ CONVSPSO
CONVHADOUI /*CONVHADNON*/ CONVHADSO 

INF24OUI /*INF24NON*/ INF24NR
DIFRECRUTOUI

/*propHAD propASH propALZ propPH*/ 
GMP_inc GMP_m600 GMP_600649 /*GMP_650699*/ GMP_700749 GMP_750799 GMP_800p 
PMP_inc PMP_Q4 PMP_Q3 /*PMP_Q2*/ PMP_Q1 
PUI_OUI
PASOINSCOM_OUI
PADEPCOM_OUI
ACCFV_OUI
PROETSOINSP_OUI
FVIE_OUI
CONVSOINSP_OUI
propASH_inc propASH_0 propASH_110 /*propASH_1120*/ propASH_2130 propASH_3150 propASH_50p
/*GMPcarre*/
/************** 3 - localisation */
/*degreUbis_inc*/ degreUbis_1 /*degreUbis_2*/ degreUbis_34
reorder. ;
CLASS Finessetab
 homme /*AGESOR5_AN AGECARRE */
AGE_m70 AGE_7079 AGE_8084 /*AGE_8589*/ AGE_9094 AGE_9599 AGE_p100 AGE_INC
GIRS1 GIRS2 /*GIRS3*/ GIRS4 GIRS5 GIRS6 GIRSNA 
handicap
asUHR asALZ asPVH asPASA /*asNONE*/ 
/*DUREE_ans 1688 missing g�n�r�s ici*/
duree_inc duree_02 duree_35 duree_611 duree_1218 /*duree_1928*/ duree_2938 duree_3949 duree_5067 duree_6896 duree_97p 
/***************** 2 - caract�ristiques de l'�tab */
categPRL /*categPRNL*/ categPUH categPUNH
CONVSPOUI /*CONVSPNON*/ CONVSPSO
CONVHADOUI /*CONVHADNON*/ CONVHADSO 

INF24OUI /*INF24NON*/ INF24NR
DIFRECRUTOUI

/*propHAD propASH propALZ propPH*/ 
GMP_inc GMP_m600 GMP_600649 /*GMP_650699*/ GMP_700749 GMP_750799 GMP_800p 
PMP_inc PMP_Q4 PMP_Q3 /*PMP_Q2*/ PMP_Q1 
PUI_OUI
PASOINSCOM_OUI
PADEPCOM_OUI
ACCFV_OUI
PROETSOINSP_OUI
FVIE_OUI
CONVSOINSP_OUI
propASH_inc propASH_0 propASH_110 /*propASH_1120*/ propASH_2130 propASH_3150 propASH_50p
/*GMPcarre*/

/************** 3 - localisation */
/*degreUbis_inc*/ degreUbis_1 /*degreUbis_2*/ degreUbis_34;

MODEL &varexp.(DESCENDING)  = homme /*AGESOR5_AN AGECARRE */
AGE_m70 AGE_7079 AGE_8084 /*AGE_8589*/ AGE_9094 AGE_9599 AGE_p100 AGE_INC
GIRS1 GIRS2 /*GIRS3*/ GIRS4 GIRS5 GIRS6 GIRSNA 
handicap
asUHR asALZ asPVH asPASA /*asNONE*/ 
/*DUREE_ans 1688 missing g�n�r�s ici*/
duree_inc duree_02 duree_35 duree_611 duree_1218 /*duree_1928*/ duree_2938 duree_3949 duree_5067 duree_6896 duree_97p 


/***************** 2 - caract�ristiques de l'�tab */

categPRL /*categPRNL*/ categPUH categPUNH
CONVSPOUI /*CONVSPNON*/ CONVSPSO
CONVHADOUI /*CONVHADNON*/ CONVHADSO 

INF24OUI /*INF24NON*/ INF24NR
DIFRECRUTOUI

/*propHAD propASH propALZ propPH*/ 
GMP_inc GMP_m600 GMP_600649 /*GMP_650699*/ GMP_700749 GMP_750799 GMP_800p 
PMP_inc PMP_Q4 PMP_Q3 /*PMP_Q2*/ PMP_Q1 
PUI_OUI
PASOINSCOM_OUI
PADEPCOM_OUI
ACCFV_OUI
PROETSOINSP_OUI
FVIE_OUI
CONVSOINSP_OUI
propASH_inc propASH_0 propASH_110 /*propASH_1120*/ propASH_2130 propASH_3150 propASH_50p
/*GMPcarre*/

/************** 3 - localisation */
/*degreUbis_inc*/ degreUbis_1 /*degreUbis_2*/ degreUbis_34

/ DIST=BINARY LINK=logit SOLUTION ;

RANDOM INTERCEPT / SUBJECT = Finessetab /*SOLUTION*/;

RUN;


/*************** 7 : au niveau �tablissement. Variable d�pendante = PROPORTION DE DECES A L'HOPITAL SUR L'ENSEMBLE DES DECES */

proc means data=deces01b ;
output out=blip; class finessetab; var 
homme AGESOR5_AN
GIRS1 GIRS2 /*GIRS3*/ GIRS4 GIRS5 GIRS6 GIRSNA 
handicap
asUHR asALZ asPVH asPASA /*asNONE*/ 
DUREE_ans ; 
run; 


data moyennes; set blip; if ((_TYPE_=1) and (_STAT_="MEAN"));
rename 
GIRS1=	moy_GIRS1
GIRS2=	moy_GIRS2
GIRS4=	moy_GIRS4
GIRS5=	moy_GIRS5
GIRS6=	moy_GIRS6
GIRSNA=	moy_GIRSNA
asALZ=	moy_asALZ
asPASA=	moy_asPASA
asPVH=	moy_asPVH
asUHR=	moy_asUHR
duree_ans=	moy_duree_ans
handicap=	moy_handicap
homme=	moy_homme
AGESOR5_AN=moy_AGESOR5;
run;
proc contents data=moyennes; run; 
proc sort data=deces01b ; BY  finessetab; RUN;
data paretab0; set deces01b; by FINESSETAB;
retain nbdeces 0;if first.FINESSETAB then nbdeces=1;
else nbdeces=nbdeces+1;

retain nbdcH 0;if first.FINESSETAB then nbdcH=deceshosp;
else nbdcH=nbdcH+deceshosp;
propH=nbdcH/nbdeces;

retain nbdcHYC 0;if first.FINESSETAB then nbdcHYC=dcycSLDMCO;
else nbdcHYC=nbdcHYC+dcycSLDMCO;
propHYC=nbdcHYC/nbdeces;
run;
data mesetabs; set paretab0; by finessetab; 
if last.finessetab; run; 
proc means data=mesetabs; var propH propHYC; class categ; run; 
/* on voit que la diff�rence est tr�s marqu�e pour le public hospitalier */

proc sort data=mesetabs; by FINESSETAB; run;
proc sort data=moyennes; by FINESSETAB; run;

data tt_etab; merge mesetabs moyennes; by FINESSETAB; run;

proc reg data=tt_etab; 
model propHYC = 
moy_homme
moy_AGESOR5
moy_GIRS1
moy_GIRS2
moy_GIRS4
moy_GIRS5
moy_GIRS6
moy_GIRSNA
moy_handicap
moy_asUHR
moy_asALZ
moy_asPVH
moy_asPASA
moy_duree_ans

categPRL /*categPRNL*/ categPUH categPUNH
CONVSPOUI /*CONVSPNON*/ CONVSPSO
CONVHADOUI /*CONVHADNON*/ CONVHADSO 

INF24OUI /*INF24NON*/ INF24NR
DIFRECRUTOUI

GMP_inc GMP_m600 GMP_600649 /*GMP_650699*/ GMP_700749 GMP_750799 GMP_800p 
PMP_inc PMP_Q4 PMP_Q3 /*PMP_Q2*/ PMP_Q1 
PUI_OUI
PASOINSCOM_OUI
PADEPCOM_OUI
ACCFV_OUI
PROETSOINSP_OUI
FVIE_OUI
CONVSOINSP_OUI

propASH_inc propASH_0 propASH_110 /*propASH_1120*/ propASH_2130 propASH_3150 propASH_50p
/*GMPcarre*/

/************** 3 - localisation */
/*degreUbis_inc*/ degreUbis_1 /*degreUbis_2*/ degreUbis_34; 
run; quit;

/* pour m�moire, le format */

proc format; 
value $type
"01"="Domicile priv� ou d�un proche"
"02"="Accueil familial agr��"
"03"="Logement-foyer"
"04"="Maison de retraite non EHPAD"
"05"="Maison de retraite m�dicalis�e, EHPAD (hors UHR)"
"06"="UHR d'une maison de retraite m�dicalis�e ou d'un EHPAD"
"07"="Soins de longue dur�e"
"08"="Service de soins de suite et de r�adaptation d�un �tablissement de sant� (ex moyen s�jour) (hors UCC)"
"09"="Unit� cognitivo-comportementale (UCC)"
"10"="Unit� de court s�jour (�m�decine, chirurgie�)"
"11"="�tablissement psychiatrique ou service psychiatrique d�un �tablissement de sant�"
"12"="�tablissement pour adultes handicap�s"
"13"="Autre"
"99"="Ne sait pas"
""="Non renseign�";
run;


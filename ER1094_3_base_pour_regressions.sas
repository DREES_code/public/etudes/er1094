/* Copyright (C) 2018. Logiciel �labor� par l��tat, via la Drees.

Auteurs : Marianne Muller et Delphine Roy, Drees.

Ce programme informatique a �t� d�velopp� par la Drees. Il permet de produire les illustrations de la publication n�1094
de la collection �tudes et r�sultats, "L�Ehpad, dernier lieu de vie pour un quart des personnes d�c�d�es en France en 2015".

Le texte et les tableaux de l�article peuvent �tre consult�s sur le site de la DREES : 
https://drees.solidarites-sante.gouv.fr/etudes-et-statistiques/publications/etudes-et-resultats/article/l-ehpad-dernier-lieu-de-vie-pour-un-quart-des-personnes-decedees-en-france-en

Ce programme utilise les donn�es des �ditions 2011 et 2015 de l'enqu�te EHPA de la DREES. 
Dans les donn�es utilis�es, les �tablissements sont rep�r�s par leur num�ro FINESS (variable finessetab), qui n'est diffus� que via le CASD. 
Les analyses peuvent toutefois �tre r�pliqu�es sur les donn�es diffus�es via la plateforme PROGEDO (r�seau Quetelet), 
en rempla�ant cette variable par le num�ro d'�tablissement anonymis�.

Il utilise �galement une variable DEGRE_UNIT pour classer les communes selon leur caract�re urbain ou rural, qui n'est actuellement pas diffus�e. 

Bien qu�il n�existe aucune obligation l�gale � ce sujet, les utilisateurs de ce programme sont invit�s � 
signaler � la DREES leurs travaux issus de la r�utilisation de ce code, ainsi que les �ventuels probl�mes 
ou anomalies qu�ils y rencontreraient, en �crivant � l�adresse �lectronique DREES-CODE@sante.gouv.fr

Ce programme a �t� ex�cut� pour la derni�re fois le 03/09/2020 avec la version 9.4 de SAS.

================================================================================================

# LICENCE : Ce logiciel est r�gi par la licence �GNU General Public License� GPL-3.0. 
# https://spdx.org/licenses/GPL-3.0.html#licenseText

# � cet �gard l'attention de l'utilisateur est attir�e sur les risques associ�s au chargement, � l'utilisation,
# � la modification et/ou au d�veloppement et � la reproduction du logiciel par l'utilisateur �tant donn� sa 
# sp�cificit� de logiciel libre, qui peut le rendre complexe � manipuler et qui le r�serve donc � des 
# d�veloppeurs et des professionnels avertis poss�dant des connaissances informatiques approfondies. 
# Les utilisateurs sont donc invit�s � charger et tester l'ad�quation du logiciel � leurs besoins dans des 
# conditions permettant d'assurer la s�curit� de leurs syst�mes et ou de leurs donn�es et, plus g�n�ralement, 
# � l'utiliser et l'exploiter dans les m�mes conditions de s�curit�.

# Le fait que vous puissiez acc�der � cet en-t�te signifie que vous avez pris connaissance de la licence GPL-3.0, et que vous en avez accept� les termes.

# This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
# This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
# You should have received a copy of the GNU General Public License along with this program.If not, see <https://www.gnu.org/licenses/>.

================================================================================================ */

/*********************************************************************************************************************************************************************************/
/************************** ER 1094 - programme 3 - cr�ation des variables pour mod�liser la probabilit� de d�c�der dans l'EHPAD plut�t qu'� l'h�pital ***************************/
/*********************************************************************************************************************************************************************************/

/*********************************************** 2.1. PROGRAMME CONSTRUISANT LA BASE POUR LES REGRESSIONS ***************************************/
libname EHPA "[]\Bases nat avec pond�ration r�gionale_sept 2017";
libname EHPA2011 "[]\Bases 2011 avec CATEG_2";
libname deces '[]\Bases ER1049';


/* variables soins palliatifs � aller r�cup�rer dans la table 1 */
data ett; set ehpa.f1_ett;
if categ_2 in ("EHPAD priv�s � but lucratif" "EHPAD priv�s � but non lucratif" "EHPAD publics hospitaliers" "EHPAD publics non hospitaliers");
/*where cat="500";*/
keep finessetab CONVMOB BENEFMOB CONVRES BENEFRES CONVHAD BENEFHAD BENEF_ASH PUI POIDS1;
run;

/* variables �tablissements � aller r�cup�rer dans la table 2 */
data acti; set ehpa.f2_acti; 
where categ_2 in ("EHPAD priv�s � but lucratif" "EHPAD priv�s � but non lucratif" "EHPAD publics hospitaliers" "EHPAD publics non hospitaliers");
keep finessetab pitot alztot handtot sortot GMP PMP EFFTOT PASOINS PADEP PASOINSCOM PADEPCOM ACCFV PROETSOINSP FVIE CONVSOINSP;
run;
/* A noter : Concernant les effectifs accueillis/sortis (EFFTOT), ne prendre que ceux en h�bergement permanent */

/* variables sur le personnel � aller r�cup�rer dans la table 3a */
data per1; set ehpa.f3a_per1;
where categ_2 in ("EHPAD priv�s � but lucratif" "EHPAD priv�s � but non lucratif" "EHPAD publics hospitaliers" "EHPAD publics non hospitaliers");
length INF24 $1.;
	INF24="";
	if VEILLE="0" then INF24="0";
	if VEILLE="1" then do;
		if (NSINF>0 and NWEINF>0 and JWEINF>0) then INF24="1";
		if (NSINF=0 or NWEINF=0 or JWEINF=0) then INF24="0";
	end; 
run; 
/*proc freq; table inf24*categ_2 / missing; run; - c'est massivement un truc de pubhosp. Et 13% de NR � ne pas oublier en modalit� */

/* ajout du 12/11/18 : pourcentage d'Ehpad avec infirmi�re 24h/24  */
proc freq; table inf24*categ_2 / missing; weight poids3A; run; /* 9% dont c'est s�r qu'ils en ont une  */
proc freq; table inf24*categ_2 ; weight poids3A; run; /* 11% sur le champ des seuls r�pondants  */


data per2; set per1; keep finessetab DIFRECRUT INF24; run; 

/* R�sidents sortis d'EHPAD, en h�bergement permanent */
/* A noter : pour pouvoir comparer les r�sultats, on laisse les sorties autres que d�c�s. */
/* Pour rep�rer les d�c�s : where motsor in ("1" "2" "3") */

data sor; set ehpa.f5_sor; 
where categ_2 in ("EHPAD priv�s � but lucratif" "EHPAD priv�s � but non lucratif" "EHPAD publics hospitaliers" "EHPAD publics non hospitaliers") and HBGT5="1";
run;

proc sort data=acti; by finessetab; run; 
proc sort data=ett; by finessetab; run; 
data etabsF1F2; merge ett(in=f1) acti (in=f2); by finessetab; 
Fiche1=f1; Fiche2 = f2; run;

proc sort data=champtot_bis; by FINESSETAB; run;
proc sort data=etabsF1F2; by FINESSETAB; run;
data etabsOK; merge champtot_bis etabsF1F2; by FINESSETAB; run;

proc sort data=etabsOK; by finessetab; run;
proc sort data=per2; by finessetab; run;
data etabsOK2; merge etabsOK per2; by finessetab; run;

proc sort data=etabsOK2; by finessetab; run;
proc sort data=sor; by finessetab; run;
data sorbis; merge etabsOK2 (in=a) sor (in=b); if b; by finessetab; /* etabOK=a; */ run;


/* r�cup�ration de la variable du programme pr�c�dent, pour v�rifier que �a donne bien la m�me chose */
/* + modification pour inclure les NR dans le 0 par d�faut */
data sorter; set sorbis; 
length CONVSP $1.;
CONVSP="0";
if CONVMOB="2" or CONVRES="2" then CONVSP="2";
if CONVMOB="1" or CONVRES="1" then CONVSP="1";
/*if CONVMOB="" and CONVRES="" then CONVSP="0";*/
run;
proc freq; table convsp / missing; run;

/********************************** Formats ******************************************/

proc format; 
value $gir
"1"="1"
"2"="2"
"3"="3"
"4"="4"
"5"="5"
"6"="6"
"  ","9"="Non renseign�";

value $sexe
"1"="1 Hommes"
"2"="2 Femmes"
" "="3 Non renseign� "; 

value $HBTG
"1"="H�bergement permanent"
"2"="H�bergement temporaire"
"3"="Accueil de jour"
"4"="Accueil de nuit";

value $motsor
"1"="1A	D�c�s dans l��tablissement" 
"2"="1B	D�c�s lors d�une hospitalisation" 
"3"="1C	D�c�s lors d�une autre sortie temporaire"
"4"="2	D�part volontaire � l�initiative du r�sident ou d�un proche"
"5"="3	Fin d'un s�jour volontaire � l'initiative du r�sident ou d'un proche"
"6"="4	R�siliation du Contrat de s�jour � l�initiative de l�ET_inadaptation de l��tat de sant�" 
"7"="5	R�siliation du Contrat de s�jour � l�initiative de l�ET_d�faut de paiement" 
"8"="6	Autre"
" "="Non renseign�";

value $motsorC
"1"="1A	D�c�s dans l��tablissement" 
"2"="1B	D�c�s lors d�une hospitalisation" 
"3"="1C	D�c�s lors d�une sortie"
"4"="Sortie sans d�c�s"
"5"="Sortie sans d�c�s"
"6"="Sortie sans d�c�s" 
"7"="Sortie sans d�c�s" 
"8"="Sortie sans d�c�s"
" "="";

value $type
"01"="Domicile priv� ou d�un proche"
"02"="Accueil familial agr��"
"03"="Logement-foyer"
"04"="Maison de retraite non EHPAD"
"05"="Maison de retraite m�dicalis�e, EHPAD (hors UHR)"
"06"="UHR d'une maison de retraite m�dicalis�e ou d'un EHPAD"
"07"="Soins de longue dur�e"
"08"="Service de soins de suite et de r�adaptation d�un �tablissement de sant� (ex moyen s�jour) (hors UCC)"
"09"="Unit� cognitivo-comportementale (UCC)"
"10"="Unit� de court s�jour (�m�decine, chirurgie�)"
"11"="�tablissement psychiatrique ou service psychiatrique d�un �tablissement de sant�"
"12"="�tablissement pour adultes handicap�s"
"13"="Autre"
"99"="Ne sait pas"
""="Non renseign�";

run;

proc freq data=sorter; table motsor*dest / missing; format motsor $motsor. dest $type.; run; 
/* sans pond�ration 76% d�c�dent dans l'EHPAD et 24% lors d'une hospitalisation */
/* avec pond�rations, idem: 24/76 */

/* rappel:  il faudra regarder ceux qui sortent vers une USLD car ils repr�setent tout de m�me 10% */

data deces01; set sorter; 
if motsor="1" then decesla=1; 
if motsor="2" then decesla=0;

if motsor="2" then deceshosp=1; 
if motsor="1" then deceshosp=0;

dcycSLDMCO=deceshosp; 
if dest in ("07","10") then dcycSLDMCO=1; 
 
agarder=0; 
if motsor in ("1","2")then agarder=1; 
if dest in ("07","10") then agarder=1;

moisdeces=SUBSTR(SOR5,4,2);

AGESOR5_AN=INT(AGESOR5/12);
if AGESOR5=. then AGESOR5_AN=.;
 
LENGTH AGESTR $5.; 
IF 1<AGESOR5_AN<70 then AGESTR='<70';
IF 69<AGESOR5_AN<80 then AGESTR='70-79';
IF 79<AGESOR5_AN<85 then AGESTR='80-84';
IF 84<AGESOR5_AN<90 then AGESTR='85-89';
IF 89<AGESOR5_AN<95 then AGESTR='90-94';
IF 94<AGESOR5_AN<100 then AGESTR='95-99';
IF 99<AGESOR5_AN then AGESTR='100+';
IF AGESOR5_AN=. then AGESTR='NA';

propHAD=(BENEFHAD*100/EFFTOT);
/*if propHAD=. then propHAD=0; */
propASH=(BENEF_ASH*100/EFFTOT);
/*if propASH=. then propASH=0; */
propALZ=(ALZTOT*100/EFFTOT);
/*if propALZ=. then propALZ=0; */
propPH=(handtot*100/EFFTOT);
/*if propPH=. then propPH=0; */
run;

data deces.ttessorties; set deces01; run;

proc freq; where agarder=1; table decesla*deceshosp / missing; run;
proc freq; where agarder=1; table deceshosp*dcycSLDMCO / missing; run;
proc freq; table 
PASOINS PADEP PASOINSCOM PADEPCOM ACCFV PROETSOINSP FVIE CONVSOINSP / missing; run;
proc freq; table PASOINS*PASOINSCOM  / list missing; run; 
/* on garde pasoinscom, qui a l'air de refl�ter davantage les pratiques que la th�orie */
proc freq; table  PADEP*PADEPCOM  / missing; run; /* idem */


proc freq data=deces01; table degre_unit / missing; run;

data deces01b; set deces01; if agarder=1; 

dureebis=duree5; 
/*  1 - CACARTERISTIQUES DU RESIDENT */
/* SEXE */
femme=(sexe5="2");
homme=(sexe5="1");
/* AGE */
AGECARRE=AGESOR5_AN*AGESOR5_AN;

duree_ans=duree5/12; 
AGE_m70=(AGESTR='<70');
AGE_7079=(AGESTR='70-79');
AGE_8084=(AGESTR='80-84');
AGE_8589=(AGESTR='85-89');
AGE_9094=(AGESTR='90-94');
AGE_9599=(AGESTR='95-99');
AGE_p100=(AGESTR='100+');
AGE_INC=(AGESTR='NA');

/* GIR */
GIRS1=(gir5="1");
GIRS2=(gir5="2");
GIRS3=(gir5="3");
GIRS4=(gir5="4");
GIRS5=(gir5="5");
GIRS6=(gir5="6");
GIRSNA=(gir5 in ("  ","9","Non renseign�"));

/* Le r�sident �tait en accueil sp�cifique */
asUHR=(ACC_SPE5="1");
asALZ=(ACC_SPE5="2");
asPVH=(ACC_SPE5="3");
asPASA=(ACC_SPE5="4");
asNONE=(ACC_SPE5 in ("5","","9"));

/* handicap rep�r� avant 60 ans*/
handicap=(hand5="1");

/* dur�e de s�jout */
duree_inc=(duree5=.);
duree_02=(0=<duree5<3);
duree_35=(3=<duree5<6);
duree_611=(6=<duree5<12);
duree_1218=(12=<duree5<19);
duree_1928=(19=<duree5<29); /* la plus nbreuse, mettre en ref */
duree_2938=(29=<duree5<39);
duree_3949=(39=<duree5<50);
duree_5067=(50=<duree5<68);
duree_6896=(68=<duree5<97);
duree_97p=(97=<duree5);

/*  2 - CACARTERISTIQUES DE L'ETABLISSEMENT  */

categPRL=categ_2 in ("EHPAD priv�s � but lucratif" );
categPRNL=categ_2 in ("EHPAD priv�s � but non lucratif" );
categPUH=categ_2 in ("EHPAD publics hospitaliers");
categPUNH=categ_2 in ("EHPAD publics non hospitaliers");

CONVSPOUI=(CONVSP="1");
CONVSPNON=(CONVSP="0");
CONVSPSO=(CONVSP="2");

CONVHADOUI=(CONVHAD="1"); 
CONVHADNON=(CONVHAD="2"); 
CONVHADSO=(CONVHAD=""); 

PUI_OUI=(PUI='1'); 
PASOINSCOM_OUI=(PASOINSCOM='1');
PADEPCOM_OUI=(PADEPCOM='1');
ACCFV_OUI=(ACCFV='1');
PROETSOINSP_OUI=(PROETSOINSP='1');
FVIE_OUI=(FVIE='1');
CONVSOINSP_OUI=(CONVSOINSP='1');

propASH_inc=(propASH=.);
propASH_0=(propASH=0);
propASH_110=(0<propASH=<10);
propASH_1120=(10<propASH=<20);
propASH_2130=(20<propASH=<30);
propASH_3150=(30<propASH=<50);
propASH_50p=(50<propASH);

/* cr�er une modalit� "manquant" car sinon il y a bcp trop de missing */
GMP_inc=(GMP=.);
GMP_m600=(1<GMP<600);
GMP_600649=(599<GMP<650);
GMP_650699=(649<GMP<700);
GMP_700749=(699<GMP<750);
GMP_750799=(749<GMP<800);
GMP_800p=(799<GMP);
GMPcarre=GMP*GMP;

PMP_inc=(PMP=.);
PMP_Q4=(PMP>230); 
PMP_Q3=(201<PMP<231);
PMP_Q2=(174<PMP<202); 
PMP_Q1=(0<PMP<175); 

/* REGION */
reg13=(nouvreg_code='13');
reg14=(nouvreg_code='14');
reg20=(nouvreg_code='20');
reg21=(nouvreg_code='21');
reg33=(nouvreg_code='33');
reg34=(nouvreg_code='34');
reg35=(nouvreg_code='35');
reg44=(nouvreg_code='44');
reg45=(nouvreg_code='45');
reg54=(nouvreg_code='54');
reg59=(nouvreg_code='59');
reg69=(nouvreg_code='69');
reg75=(nouvreg_code='75');
regDOM=(nouvreg_code in('91','92','93','94'));

TUU_0=(tuu2012='0');
TUU_1=(tuu2012='1');
TUU_2=(tuu2012='2');
TUU_3=(tuu2012='3');
TUU_4=(tuu2012='4');
TUU_5=(tuu2012='5');
TUU_6=(tuu2012='6');
TUU_7=(tuu2012='7');
TUU_8=(tuu2012='8');

/* ajouts octobre */
INF24OUI=(inf24=1);
INF24NON=(inf24=0);
INF24NR=(inf24=.);

DIFRECRUTOUI=(difrecrut=1);

degreU_inc=(DEGRE_UNIT=.);
degreU_1=(DEGRE_UNIT=1);
degreU_2=(DEGRE_UNIT=2);
degreU_3=(DEGRE_UNIT=3);
degreU_4=(DEGRE_UNIT=4);

/*1- communes dens�ment peupl�es ;
2- communes de densit� interm�diaire ;
3- communes peu denses ;
4- communes tr�s peu denses. */

run;

data deces.deces01b; set deces01b; run;

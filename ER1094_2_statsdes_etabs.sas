/* Copyright (C) 2018. Logiciel �labor� par l��tat, via la Drees.

Auteurs : Marianne Muller et Delphine Roy, Drees.

Ce programme informatique a �t� d�velopp� par la Drees. Il permet de produire les illustrations de la publication n�1094
de la collection �tudes et r�sultats, "L�Ehpad, dernier lieu de vie pour un quart des personnes d�c�d�es en France en 2015".

Le texte et les tableaux de l�article peuvent �tre consult�s sur le site de la DREES : 
https://drees.solidarites-sante.gouv.fr/etudes-et-statistiques/publications/etudes-et-resultats/article/l-ehpad-dernier-lieu-de-vie-pour-un-quart-des-personnes-decedees-en-france-en

Ce programme utilise les donn�es des �ditions 2011 et 2015 de l'enqu�te EHPA de la DREES. 
Dans les donn�es utilis�es, les �tablissements sont rep�r�s par leur num�ro FINESS (variable finessetab), qui n'est diffus� que via le CASD. 
Les analyses peuvent toutefois �tre r�pliqu�es sur les donn�es diffus�es via la plateforme PROGEDO (r�seau Quetelet), en rempla�ant cette variable par le num�ro d'�tablissement anonymis�.

Bien qu�il n�existe aucune obligation l�gale � ce sujet, les utilisateurs de ce programme sont invit�s � 
signaler � la DREES leurs travaux issus de la r�utilisation de ce code, ainsi que les �ventuels probl�mes 
ou anomalies qu�ils y rencontreraient, en �crivant � l�adresse �lectronique DREES-CODE@sante.gouv.fr

Ce programme a �t� ex�cut� pour la derni�re fois le 03/09/2020 avec la version 9.4 de SAS.

================================================================================================

# LICENCE : Ce logiciel est r�gi par la licence �GNU General Public License� GPL-3.0. # https://spdx.org/licenses/GPL-3.0.html#licenseText

# � cet �gard l'attention de l'utilisateur est attir�e sur les risques associ�s au chargement, � l'utilisation,
# � la modification et/ou au d�veloppement et � la reproduction du logiciel par l'utilisateur �tant donn� sa 
# sp�cificit� de logiciel libre, qui peut le rendre complexe � manipuler et qui le r�serve donc � des 
# d�veloppeurs et des professionnels avertis poss�dant des connaissances informatiques approfondies. 
# Les utilisateurs sont donc invit�s � charger et tester l'ad�quation du logiciel � leurs besoins dans des 
# conditions permettant d'assurer la s�curit� de leurs syst�mes et ou de leurs donn�es et, plus g�n�ralement, 
# � l'utiliser et l'exploiter dans les m�mes conditions de s�curit�.

# Le fait que vous puissiez acc�der � cet en-t�te signifie que vous avez pris connaissance de la licence GPL-3.0, et que vous en avez accept� les termes.

# This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
# This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
# You should have received a copy of the GNU General Public License along with this program.If not, see <https://www.gnu.org/licenses/>.

================================================================================================ */

/*********************************************************************************************************************************************/

libname EHPA "[]\Bases nat avec pond�ration r�gionale_sept 2017";
libname EHPA2011 "[]\Bases 2011 avec CATEG_2";

/*****************************************************************************************************************************************************/
/*************************** ER 1094 - programme 2 - CREATION DE LA BASE ET STATISTIQUES DESCRIPTIVES SUR LES ETABLISSEMENTS *************************/
/*****************************************************************************************************************************************************/

/* EHPAD */
data acti; set ehpa.f2_acti; 
where categ_2 in ("EHPAD priv�s � but lucratif" "EHPAD priv�s � but non lucratif" "EHPAD publics hospitaliers" "EHPAD publics non hospitaliers");
run;
/* A noter : Concernant les effectifs accueillis/sortis (EFFTOT), ne prendre que ceux en h�bergement permanent */

data ett; set ehpa.f1_ett; 
where categ_2 in ("EHPAD priv�s � but lucratif" "EHPAD priv�s � but non lucratif" "EHPAD publics hospitaliers" "EHPAD publics non hospitaliers");
run;

/* R�sidents en EHPAD, en h�bergement permanent */
data acc; set ehpa.f4_acc; 
where categ_2 in ("EHPAD priv�s � but lucratif" "EHPAD priv�s � but non lucratif" "EHPAD publics hospitaliers" "EHPAD publics non hospitaliers") and HBTG4="1";
run;

/* R�sidents en EHPAD, en h�bergement permanent */
/* A noter : pour pouvoir comparer les r�sultats, on laisse les sorties autres que d�c�s. 
Pour rep�rer les d�c�s : where motsor in ("1" "2" "3") */

data sor; set ehpa.f5_sor; 
where categ_2 in ("EHPAD priv�s � but lucratif" "EHPAD priv�s � but non lucratif" "EHPAD publics hospitaliers" "EHPAD publics non hospitaliers") and HBGT5="1";
run;


/*** 2011 ***/

/* Fiche �tab */
data ett2011; set ehpa2011.ett_2011; 
where categ_2 in ("EHPAD priv�s � but lucratif" "EHPAD priv�s � but non lucratif" "EHPAD publics hospitaliers" "EHPAD publics non hospitaliers");
run;

data acti2011; set ehpa2011.acti_2011; 
where categ_2 in ("EHPAD priv�s � but lucratif" "EHPAD priv�s � but non lucratif" "EHPAD publics hospitaliers" "EHPAD publics non hospitaliers");
run;

data acc2011; set ehpa2011.acc_2011; 
where categ_2 in ("EHPAD priv�s � but lucratif" "EHPAD priv�s � but non lucratif" "EHPAD publics hospitaliers" "EHPAD publics non hospitaliers") and HBGT4="1";
run;


data sor2011; set ehpa2011.sor_2011; 
where categ_2 in ("EHPAD priv�s � but lucratif" "EHPAD priv�s � but non lucratif" "EHPAD publics hospitaliers" "EHPAD publics non hospitaliers") and HBGT5="1";
run;


/********************************** Formats ******************************************/

proc format; 
value $gir
"1"="1"
"2"="2"
"3"="3"
"4"="4"
"5"="5"
"6"="6"
"  ","9"="Non renseign�";

value $sexe
"1"="1 Hommes"
"2"="2 Femmes"
" "="3 Non renseign� "; 

value $HBTG
"1"="H�bergement permanent"
"2"="H�bergement temporaire"
"3"="Accueil de jour"
"4"="Accueil de nuit";

value $motsor
"1"="1A	D�c�s dans l��tablissement" 
"2"="1B	D�c�s lors d�une hospitalisation" 
"3"="1C	D�c�s lors d�une autre sortie temporaire"
"4"="2	D�part volontaire � l�initiative du r�sident ou d�un proche"
"5"="3	Fin d'un s�jour volontaire � l'initiative du r�sident ou d'un proche"
"6"="4	R�siliation du Contrat de s�jour � l�initiative de l�ET_inadaptation de l��tat de sant�" 
"7"="5	R�siliation du Contrat de s�jour � l�initiative de l�ET_d�faut de paiement" 
"8"="6	Autre"
" "="Non renseign�";


value $motsorC
"1"="1A	D�c�s dans l��tablissement" 
"2"="1B	D�c�s lors d�une hospitalisation" 
"3"="1C	D�c�s lors d�une sortie"
"4"="Sortie sans d�c�s"
"5"="Sortie sans d�c�s"
"6"="Sortie sans d�c�s" 
"7"="Sortie sans d�c�s" 
"8"="Sortie sans d�c�s"
" "="";


value $type
"01"="Domicile priv� ou d�un proche"
"02"="Accueil familial agr��"
"03"="Logement-foyer"
"04"="Maison de retraite non EHPAD"
"05"="Maison de retraite m�dicalis�e, EHPAD (hors UHR)"
"06"="UHR d'une maison de retraite m�dicalis�e ou d'un EHPAD"
"07"="Soins de longue dur�e"
"08"="Service de soins de suite et de r�adaptation d�un �tablissement de sant� (ex moyen s�jour) (hors UCC)"
"09"="Unit� cognitivo-comportementale (UCC)"
"10"="Unit� de court s�jour (�m�decine, chirurgie�)"
"11"="�tablissement psychiatrique ou service psychiatrique d�un �tablissement de sant�"
"12"="�tablissement pour adultes handicap�s"
"13"="Autre"
"99"="Ne sait pas"
""="Non renseign�";

value $TUU
"0"="Commune rurale" 
"1"="Commune appartenant � une unit� urbaine de 2 000 � 4 999 habitants"
"2"="Commune appartenant � une unit� urbaine de 5 000 � 9 999 habitants" 
"3"="Commune appartenant � une unit� urbaine de 10 000 � 19 999 habitants" 
"4"="Commune appartenant � une unit� urbaine de 20 000 � 49 999 habitants" 
"5"="Commune appartenant � une unit� urbaine de 50 000 � 99 999 habitants" 
"6"="Commune appartenant � une unit� urbaine de 100 000 � 199 999 habitants" 
"7"="Commune appartenant � une unit� urbaine de 200 000 � 1 999 999 habitants" 
"8"="Commune appartenant � l'unit� urbaine de Paris"
" "="Non renseign�";

value $DEST 
""="Non r�ponse"
"01"="Domicile priv� ou d�un proche"
"02"="Accueil familial agr��"
"03"="Logement-foyer"
"04"="Maison de retraite non EHPAD"
"05"="Maison de retraite m�dicalis�e, EHPAD (hors UHR)"
"06"="UHR d'une maison de retraite m�dicalis�e ou d'un EHPAD"
"07"="Soins de longue dur�e"
"08"="Service de soins de suite et de r�adaptation d�un �tablissement de sant� (ex moyen s�jour) (hors UCC)"
"09"="Unit� cognitivo-comportementale (UCC)"
"10"="Unit� de court s�jour (�m�decine, chirurgie�)"
"11"="�tablissement psychiatrique ou service psychiatrique d�un �tablissement de sant�"
"12"="Autre"
"99"="Ne sait pas";
run;

/* comptage d�c�s par �ge en 2011 */

proc freq data=sor2011; table motsor ; weight poids5; run;
proc freq data=sor2011; where motsor in ("1","2","3"); table tragesor5; weight poids5; run;
/* 115 411 d�c�s */

/*****************************************************************************************************************************************************/
/******************************************************** PARTIE DESCRIPTIVE SUR LES ETABLISSEMENTS **************************************************/
/*****************************************************************************************************************************************************/


/***************  LES ETABLISSEMENTS : QUELLE EST L'IMPORTANCE DES DISPOSITIFS DE SOINS DE VIE ET DE SOINS PALLIATIFS ********************/
/***************** TABLEAU 3 ER ***************************/


proc freq data=ett;
table categ_2*(CONVMOB CONVRES)/nocol  nofreq missing;  
weight poids1; run;

proc freq data=ett2011;
table categ_2*(CONVMOB CONVRES)/nocol norow nopercent nocum missing; 
weight poids1; run;

/*Nombre de b�n�ficiaires d'EMSP*/
proc means data=ett sum mean;
var BENEFMOB;
where CONVMOB ne "2";
weight poids1;
run;

proc freq data=ett;
table categ_2/nocol norow nopercent missing;
*where BENEFMOB=. and CONVMOB="1" ;
*where BENEFMOB=0 and CONVMOB="1" ;
where CONVMOB="1";
weight poids1; run;


*Nombre de b�n�ficiaires de RSSP;

proc means data=ett sum mean;
var BENEFRES;
*class categ_2;
weight poids1;
run;

proc means data=ett sum mean;
var BENEFRES;
where CONVRES ne "2";
weight poids1;
run;

proc freq data=ett;
table categ/nocol norow nopercent missing;
*where BENEFRES=. and CONVRES="1";
*where BENEFRES=0 and CONVRES="1";
where CONVRES="1";
weight poids1; run;

* Nombre d'EHPAD ayant sign� une convention avec une �quipe ou un r�seau de soins palliatifs;
data ett(where=(cat="500")); set ehpa.f1_ett;
	length CONVSP $1.;
		CONVSP="0";
		if CONVMOB="2" or CONVRES="2" then CONVSP="2";
		if CONVMOB="1" or CONVRES="1" then CONVSP="1";
		if CONVMOB="" and CONVRES="" then CONVSP="";
run;

/* Autre programme pour r�sultats identiques.
data test (where=(cat="500")); set ehpa.f1_ett;
	length CONVSP $1.;
		if CONVMOB="1" or CONVRES="1" then CONVSP="1";
		if (CONVMOB="2" and CONVRES in ("0" "" "2")) or (CONVMOB in ("0" "" "2") and CONVRES="2") then CONVSP="2"; 
		if CONVMOB="0" and CONVRES="0" then CONVSP="0";
		if CONVMOB="" and CONVRES="" then CONVSP="";
run;
proc freq data=test;
table CONVSP/nocol norow nopercent; run;
proc freq data=ett; 
table CONVSP/nocol norow nopercent; run;*/


proc freq data=ett; 
table CONVRES*CONVMOB/nocol norow nopercent; run;
proc freq data=ett; 
table CONVSP/nocol norow nopercent; run;

proc freq data=ett;
table categ_2*convsp/nocum norow nopercent nocol; 
 run;


/* Rapporter le nb de b�n�ficiaires au nb de personnes d�c�d�es : /!\ ne pas prendre en compte les d�c�s ayant lieu dans les ehpad rattach�s � une struct sanitaire*/
proc sort data=ett; by finessetab; run; 
proc sort data=sor; by finessetab; run; 

data nouv;
merge ett(in=a keep=finessetab convmob convres convsp) sor(in=b);
if b;
by finessetab;
run; 

proc freq data=nouv;
table CONVSP/missing;
where motsor in ("1" "2" "3");
*weight poids5;
run; 
/*8% des sorties par d�c�s se passent dans un Ehpad rattach�s � une structure sanitaire 
=> donc 92% des d�c�s de r�sidents ont lieu dans les �tab non rattach�s � des structures sanitaires. */

/*** Pour 2011 ***/


data ett_2_2011; set ett2011;
length CONVSP $1.;
CONVSP="0";
if CONVMOB="2" or CONVRES="2" then CONVSP="2";
if CONVMOB="1" or CONVRES="1" then CONVSP="1";
if CONVMOB="" and CONVRES="" then CONVSP="";
run;


proc freq data=ett_2_2011; 
table categ_2*convmob/nocum norow nopercent nocol; 
weight poids1; run;

proc freq data=ett_2_2011; 
table categ_2*convres/nocum norow nopercent nocol; 
weight poids1; run;

proc freq data=ett_2_2011;
table categ_2*convsp/nocum norow nopercent nocol; 
weight poids1; run;


/************************* TABLEAU 4 ******************************************/
/******************** Fiche 2 : Volet fin de vie *********************************/

* ACCFV	= L'�tablissement dispose de chambres pour les personnes en fin de vie;
proc freq data=acti;
table categ_2*accfv / nocol norow nopercent missing;
weight poids2; run;


* PROETSOINSP = Volet soins palliatifs dans le projet d��tablissement;
proc freq data=acti;;
table categ_2*PROETSOINSP/nocol norow nopercent missing;
weight poids2; run;


* FVIE = Protocoles, proc�dures et/ou r�f�rentiels li�s � la fin de vie formalis�s et mis en �uvre;
proc freq data=acti;
table categ_2*FVIE/nocol norow nopercent missing;
weight poids2; run;

* CONVSOINSP = Convention avec une association de b�n�voles d'accompagnement en soins palliatifs;
proc freq data=acti;
table categ_2*CONVSOINSP/nocol norow nopercent missing;
weight poids2; run;

*DU SP; 
data acti2; set acti;
length DUSP $1;
if PERSOINSP>0 then DUSP="1";
if PERSOINSP=0 then DUSP="0";
if PERSOINSP=. then DUSP="";
run;

proc freq data=acti2;
table categ_2*DUSP/nocol norow nopercent missing;
weight poids2; run;

* Idem pour 2011;
data acti2011_2; set acti2011;
length DUSP $1;
if PERSOINSP>0 then DUSP="1";
if PERSOINSP=0 then DUSP="0";
if PERSOINSP=. then DUSP="";
run;

proc freq data=acti2011;
table categ_2*(ACCFV PROETSOINSP FVIE CONVSOINSP)/nocol norow nopercent missing;
weight poids2; run;

proc freq data=acti2011_2;
table categ_2*DUSP/nocol norow nopercent missing;
weight poids2; run;

* GMP moyen selon les dispositifs mis en place dans l'�tab niveau SP et FdV;

proc means data=acti mean q1 median q3;
var GMP; 
class PROETSOINSP;
run; 

proc means data=acti mean q1 median q3;
var GMP; 
class FVIE;
run; 

* Construction d'une base avec donn�es indiv + au niveau �tablissements.;

proc freq data=sor; 
table finessetab/out=motsor_deces;
where motsor in ("1" "2" "3"); 
*weight poids5;
run; 

proc freq data=sor; 
table finessetab/out=motsor_hosp;
where motsor in ("2"); /* D�c�s au cours d'une hospitalisation */
*weight poids5;
run; 

proc freq data=sor; 
table finessetab/out=motsor_tot;
where motsor in ("1" "2" "3" "4" "5" "6" "7" "8"); 
*weight poids5;
run; 

proc sort data=motsor_deces; by finessetab; run; 
data motsor_deces; set motsor_deces; keep finessetab count; rename count=motsor_deces; label count=motsor_deces; run; 

proc sort data=motsor_hosp; by finessetab; run;
data motsor_hosp; set motsor_hosp; keep finessetab count; rename count=motsor_hosp; label count=motsor_hosp; run; 

proc sort data=motsor_tot; by finessetab; run;
data motsor_tot; set motsor_tot; keep finessetab count; rename count=motsor_tot; label count=motsor_tot; run;  
proc sort data=acti; by finessetab; run; 
proc sort data=ett; by finessetab; run; 


data base_ett; 
merge motsor_deces(in=a) motsor_hosp(in=b) motsor_tot(in=c) acti(in=d  keep=FINESSETAB EFFTOT EFFHP EFFTEMP EFFJOUR EFFNUIT SORTOT SORTPER SORTEMP SORJOUR SORNUIT
gmp ACCFV PROETSOINSP FVIE PERSOINSP CONVSOINSP poids2) ett_bis(in=e keep=finessetab CONVSP CONVMOB BENEFMOB CONVRES BENEFRES POIDS1) ehpa.f3a_per1(in=f keep=finessetab effper effetp) ;
by finessetab; 
if e;
run;


/* NOTE: There were 4359 observations read from the data set WORK.MOTSOR_DECES.
NOTE: There were 4389 observations read from the data set WORK.MOTSOR_TOT.
NOTE: There were 5287 observations read from the data set WORK.ACTI.
NOTE: There were 5207 observations read from the data set WORK.ETT.
NOTE: The data set WORK.BASE has 5207 observations and 20 variables. */


data base_acti; 
merge motsor_deces(in=a) motsor_hosp(in=b) motsor_tot(in=c) acti(in=d  keep=FINESSETAB categ_2 EFFTOT EFFHP EFFTEMP EFFJOUR EFFNUIT SORTOT SORTPER SORTEMP SORJOUR SORNUIT
gmp ACCFV PROETSOINSP FVIE PERSOINSP CONVSOINSP pitot poids2) ett_bis(in=e keep=finessetab CONVSP CONVMOB BENEFMOB CONVRES BENEFRES TUU2012 POIDS1) ehpa.f3a_per1(in=f keep=finessetab effper effetp) ;
by finessetab; 
if d;
run;
*NOTE: The data set WORK.BASE_ACTI has 5287 observations and  29 variables.;

* Etab qui ont des convention SP, ont-ils mis en places des protocoles SPFV?;
proc sort data=base_acti; by categ_2; run; 

proc freq data=base_acti;
table FVIE*CONVSp/nocol norow nopercent nocum missing; 
*by categ_2;
weight poids2; 
run; 

* Rapport du nb de personnel avec DU en SP et nb de personnel;
proc freq data=base_acti;
table persoinsp/nocum;
weight poids2;
run; 
* 14% de NR; 
* 63% : 0 - si on ne compte pas la NR=74%; 

* Distribution du nombre de d�c�s par etab; 
proc freq data=base_acti;
table motsor_deces/nocum; run; 
run; 

data deces_estim; set base_acti; 

* Estimation nb de d�c�s ds chq ehpad;
length deces_E 4.;
if motsor_tot ne . and sortper ne . then deces_E=(motsor_deces/motsor_tot)*sortper; 
if motsor_tot=. or sortper=. then deces_E=.; 

* Estimation proportion de d�c�s parmi personnes accueillies au cours de l'ann�e 2018;
length p_deces 4.;
if effhp ne . and sortper ne . then p_deces=deces_E/(effhp+sortper)*100; 
if effhp=. or sortper=. then p_deces=.; 

length cl_p_deces $25.;
if p_deces<10 and p_deces ne . then cl_p_deces="1. Entre 0 et 10%"; 
if p_deces>=10 and p_deces<20 then cl_p_deces="2. Entre 10 et 20%"; 
if p_deces>=20 and p_deces<30 then cl_p_deces="3. Entre 20 et 30%"; 
if p_deces>=30 and p_deces<40 then cl_p_deces="4. Entre 30 et 40%"; 
if p_deces>=40 and p_deces<50 then cl_p_deces="5. Entre 40 et 50%"; 
if p_deces>=50 then cl_p_deces="6. Sup�rieur � 50%"; 
if p_deces=. then cl_p_deces=""; 
run; 

proc freq data=deces_estim;
table cl_p_deces;
weight poids2;
run; 

proc freq data=deces_estim;
table p_deces/missing;
weight poids2;
run; 

proc means data=base mean q1 median q3; 
var GMP ;
class CONVSP; 
*by categ_2;
weight poids1;
run;

proc freq data=base; table gmp/missing; weight poids1; run;

proc means data=base sum; 
var motsor_hosp motsor_deces motsor_tot EFFTOT EFFHP EFFTEMP EFFJOUR EFFNUIT SORTOT SORTPER SORTEMP SORJOUR SORNUIT;
class CONVSP; 
where motsor_deces ne . and motsor_tot ne .;
*weight poids1;
run;

proc means data=base_acti sum; 
var motsor_hosp motsor_deces motsor_tot EFFTOT EFFHP EFFTEMP EFFJOUR EFFNUIT SORTOT SORTPER SORTEMP SORJOUR SORNUIT;
class CONVSP; 
where motsor_tot>=0.9*sortot; /* on r�alise uniquement le calcul sur les �tab "suffisamment" r�pondants */
*weight poids2; *Pond�re-t-on ?;
run;
proc means data=base_acti sum; 
var motsor_hosp motsor_deces motsor_tot EFFTOT EFFHP EFFTEMP EFFJOUR EFFNUIT SORTOT SORTPER SORTEMP SORJOUR SORNUIT;
class CONVSP; 
where motsor_tot>=0.9*sortper; /* on r�alise uniquement le calcul sur les �tab "suffisamment" r�pondants */
*weight poids2; *Pond�re-t-on ?;
run;

proc means data=base_acti mean; 
var motsor_hosp motsor_deces motsor_tot EFFTOT EFFHP EFFTEMP EFFJOUR EFFNUIT SORTOT SORTPER SORTEMP SORJOUR SORNUIT GMP;
class CONVSP; 
where motsor_tot>=0.9*sortper; /* on r�alise uniquement le calcul sur les �tab "suffisamment" r�pondants */
*weight poids2; *Pond�re-t-on ?;
run;

proc means data=base_acti sum; 
var motsor_hosp motsor_deces motsor_tot EFFTOT EFFHP EFFTEMP EFFJOUR EFFNUIT SORTOT SORTPER SORTEMP SORJOUR SORNUIT GMP;
class categ_2; 
where motsor_tot>=0.9*sortper; /* on r�alise uniquement le calcul sur les �tab "suffisamment" r�pondants */
*weight poids2; 
run;


proc means data=base_acti mean; 
var motsor_hosp motsor_deces motsor_tot EFFTOT EFFHP EFFTEMP EFFJOUR EFFNUIT SORTOT SORTPER SORTEMP SORJOUR SORNUIT GMP;
class FVIE; 
where motsor_tot>=0.9*sortper; /* on r�alise uniquement le calcul sur les �tab "suffisamment" r�pondants */
*weight poids2; 
run;

proc means data=base_acti sum; 
var motsor_hosp motsor_deces motsor_tot EFFTOT EFFHP EFFTEMP EFFJOUR EFFNUIT SORTOT SORTPER SORTEMP SORJOUR SORNUIT GMP;
class FVIE; 
where motsor_tot>=0.9*sortper; /* on r�alise uniquement le calcul sur les �tab "suffisamment" r�pondants */
*weight poids2; 
run;

proc means data=base_acti mean; 
var motsor_hosp motsor_deces motsor_tot EFFTOT EFFHP EFFTEMP EFFJOUR EFFNUIT SORTOT SORTPER SORTEMP SORJOUR SORNUIT GMP;
class PROETSOINSP; 
where motsor_tot>=0.9*sortper; /* on r�alise uniquement le calcul sur les �tab "suffisamment" r�pondants */
*weight poids2; 
run;

proc means data=base_acti sum; 
var motsor_hosp motsor_deces motsor_tot EFFTOT EFFHP EFFTEMP EFFJOUR EFFNUIT SORTOT SORTPER SORTEMP SORJOUR SORNUIT GMP;
class PROETSOINSP; 
where motsor_tot>=0.9*sortper; /* on r�alise uniquement le calcul sur les �tab "suffisamment" r�pondants */
*weight poids2;
run;

* On rapatrie les donn�es �tab dans la fiche 5;
data acti2; 
set acti; 
length cl_GMP $20;
if GMP<600 and GMP ne . then cl_GMP="< 600"; 
else if GMP>=600 and GMP<700 then cl_GMP="600 - 699"; 
else if GMP>=700 and GMP<800 then cl_GMP="700 - 799";
else if GMP>=800 and GMP<900 then cl_GMP="800 - 899"; 
else if GMP>=900 then cl_GMP="900 - 1000"; 
else if GMP=. then cl_GMP="";
run; 


proc sort data=sor; by finessetab; run; 
data base_sor; 
merge sor (in=a)
acti2(in=b  keep=FINESSETAB cl_GMP ACCFV PROETSOINSP FVIE CONVSOINSP) 
ett_bis(in=c keep=finessetab CONVSP CONVMOB CONVRES TUU2012);
by finessetab; 
if a;
run;


proc sort data=acc; by finessetab; run; 
data base_acc; 
merge acc (in=a)
acti2(in=b  keep=FINESSETAB cl_GMP ACCFV PROETSOINSP FVIE CONVSOINSP) 
ett(in=c keep=finessetab CONVSP CONVMOB CONVRES);
by finessetab; 
if a;
run;

proc freq data=base_sor;
table TUU2012*MOTSOR/nocum norow nopercent nocol MISSING; 
weight poids5;
format TUU2012 $TUU.;
format motsor $motsorc.;
run; 


/*********************************** TABLEAU 5 **************************************************/
/* Raisonnement : parmi le nb de personnes d�c�d�es... combien �tait dans un �tab avec des dispositifs de SP, de FdV, ... ? */

proc freq data=base_sor;
table MOTSOR*CONVSP/nocum norow nopercent nocol MISSING; 
weight poids5;
format motsor $motsor.;
run; 


proc freq data=base_sor;
table MOTSOR*PROETSOINSP/nocum norow nopercent nocol MISSING; 
weight poids5;
format motsor $motsor.;
run; 

proc freq data=base_sor;
table MOTSOR*FVIE/nocum norow nopercent nocol MISSING; 
weight poids5;
format motsor $motsor.;
run;

proc freq data=base_sor;
table MOTSOR*cl_GMP/nocum norow nopercent nocol MISSING; 
weight poids5;
format motsor $motsorC.;
run; 

* Dur�e;
proc tabulate data=base_sor;
var duree5;
class MOTSOR cl_GMP;
table MOTSOR,cl_GMP*duree5*mean; 
weight poids5;
format motsor $motsorC.;
run; 

proc tabulate data=base_sor;
var duree5;
class cl_GMP;
table cl_GMP*duree5*mean; 
weight poids5;
format motsor $motsorC.;
run; 

proc tabulate data=base_sor;
var duree5;
class MOTSOR;
table MOTSOR*duree5*mean; 
weight poids5;
format motsor $motsorC.;
run; 

* O� vont les personnes sorties d'�tab aux GMP tr�s �lev�s ?;
proc freq data=base_sor; 
table DEST/missing;
where cl_GMP="900 - 1000" and motsor not in ("1" "2" "3");
weight poids5;
format dest $dest.;
run;


* Approfondissement : Destination des personnes qui sortent sans d�c�der ;
proc sort data=sor; by finessetab; run; 
proc sort data=acti2; by finessetab; run; 
proc sort data=ett; by finessetab; run; 

data base_sor; 
merge sor (in=a)
acti2(in=b  keep=FINESSETAB cl_GMP PROETSOINSP FVIE) ett(in=c keep=FINESSETAB CONVSP); 
by finessetab; 
if a;
run;

data base_sordest; 
set base_sor; 
length dest2 $20.;
if motsor="1" then dest2="D�c�s �tab";
else if motsor="2" then dest2="D�c�s hospi";
else if motsor="3" then dest2="D�c�s autre"; 
else if dest="01" then dest2="Domicile";
else if dest in ("02" "03" "04") then dest2="Etab non m�dic";
else if dest="05" then dest2="Ehpad";
else if dest in ("06" "08") then dest2="UHR, UCC";
else if dest="07" then dest2="SLD";
else if dest="09" then dest2="SSR";
else if dest="10" then dest2="Unit� court s�jour";
else if dest="11" then dest2="Etab psy";
else if dest="12" then dest2="Autre";
else if dest in ("99" "") and motsor not in ("1" "2" "3") then dest2="";
run;


proc freq data=base_sordest;
table dest2*categ/missing nocol norow nopercent nocum; 
weight poids5;
run;
proc freq data=base_sordest;
table dest2*CONVSP/missing nocol norow nopercent nocum; 
weight poids5;
run;
proc freq data=base_sordest;
table dest2*CONVSP/nofreq norow nopercent nocum; 
weight poids5;
run;

* DEST et CONVSP;
proc freq data=base_sordest;
table motsor*CONVSP/nofreq norow nopercent nocum; 
weight poids5;
format motsor $motsor.;
run;

* MOTSOR et CONVSP;
proc freq data=base_sordest;
table dest*CONVSP/nofreq norow nopercent nocum; 
weight poids5;
format dest $dest.;
run;

proc freq data=base_sordest;
table dest*motsor/nofreq nocol nopercent nocum; 
weight poids5;
format dest $dest. motsor $motsor.;
where CONVSP="2";
run;

proc freq data=base_sordest;
table dest/nocum; 
weight poids5;
format dest $dest. motsor $motsor.;
where motsor="6" and CONVSP="2";
run;


proc freq data=base_sordest;
table dest2*cl_GMP/nocol norow nopercent nocum; 
weight poids5;
run;
